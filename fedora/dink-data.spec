Name:		dink-data
Version:	1.08
Release:	1%{?dist}
Summary:	Adventure and role-playing game (game data)

Group:		Amusements/Games
License:	zlib
URL:		http://www.codedojo.com/
Source0:	http://www.codedojo.com/files/dink/dink_108_game_data.zip
Source1:	dink-data_README.distro
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

BuildRequires:	unzip

%description
Dink Smallwood is an adventure/role-playing game, similar to Zelda,
made by RTsoft. Besides twisted humour, it includes the actual game
editor, allowing players to create hundreds of new adventures called
Dink Modules or D-Mods for short.

This package contains architecture-independent data for the original
game (except for non-free sounds).


%prep
# missing %setup may cause some strange build failure
%setup -q -c -T
cp -p %SOURCE0 .
cp -p %SOURCE1 README


%build


%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_datadir}/dink/dink/
unzip -qq -d $RPM_BUILD_ROOT%{_datadir}/dink/dink/ dink_108_game_data.zip
mv $RPM_BUILD_ROOT%{_datadir}/dink/dink/README.txt COPYING


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_datadir}/dink/


%changelog
* Sat Aug 23 2008 Sylvain Beucler <beuc@beuc.net> 1.08-1
- Initial package
